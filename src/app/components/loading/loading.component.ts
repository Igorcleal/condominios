import { UtilsService } from './../../services/utils.service';
import { Component, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  show = false;

  constructor(private utils: UtilsService) { }

  ngOnInit() {
    this.utils.eventLoading.subscribe((res) => {
      console.log(res);
      this.show = res;
    })
  }



}
