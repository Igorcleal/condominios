import { Condominio } from "./condominio";

export class Usuario {
    id: string;
    email: string;
    idCondominio: string;

    getData(): object {
        const result = {};
        Object.keys(this).map(key => result[key] = this[key]);
        return result;
    }
}