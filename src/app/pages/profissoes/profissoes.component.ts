import { UtilsService } from './../../services/utils.service';
import { Profissao } from './../../model/profissao';
import { EnumTela } from './../../model/EnumTela';
import { CrudComponent } from './../../crud/crud.component';
import { DataTableComponent } from './../../data-table/data-table/data-table.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProfissoesListaComponent } from './profissoes-lista/profissoes-lista.component';
import { UserService } from '../../user.service';
import { ProfissoesService } from '../../services/profissoes.service';

@Component({
  selector: 'app-profissoes',
  templateUrl: './profissoes.component.html',
  styleUrls: ['./profissoes.component.scss']
})
export class ProfissoesComponent implements OnInit {

  @ViewChild(ProfissoesListaComponent)
  profissoesListaComponent: ProfissoesListaComponent;

  @ViewChild(CrudComponent)
  crudComponent: CrudComponent;

  form: FormGroup;
  public columns = [{ field: 'nome', titulo: 'Profissão' }
    , { field: 'dataCriacao', titulo: "Data Criação" }];

  profissao = new Profissao();

  idCondominio: string;

  constructor(private fb: FormBuilder,
    private userService: UserService,
    private profissoesService: ProfissoesService,
    private utils: UtilsService) { }

  ngOnInit() {
    this.idCondominio = this.userService.getIdCondominioUsuarioLogado();
    this.buildForm();

  }

  buildForm() {
    this.form = this.fb.group({
      nome: ['', [Validators.required]
      ]
    });
  }

  salvar() {
    this.utils.showLoading();
    if (this.form.valid) {

      this.profissao.nome = this.form.value.nome;

      if (this.crudComponent.tela == EnumTela.INCLUIR) {
        this.profissao.dataCriacao = new Date();

        this.profissoesService.insertProfissao(this.profissao).then((res) => {
          console.log(res);
          console.log('succeed');
          this.crudComponent.tela = EnumTela.PESQUISAR;
        }).catch(err => {
          console.error(err);
        });
      } else if (this.crudComponent.tela == EnumTela.ALTERAR) {
        console.log('alterar');
        this.profissoesService.updateProfissao(this.profissao).then((res) => {
          console.log(res);
          console.log('succeed update');
          this.crudComponent.tela = EnumTela.PESQUISAR;
        }).catch(err => {
          console.error(err);
        });
      }
    }
  }

  pesquisar() {
    this.profissoesListaComponent.pesquisar(null);
  }

  alterar(ev) {
    console.log(ev);
    this.profissao = ev;
    this.crudComponent.tela = EnumTela.ALTERAR;
  }

}
