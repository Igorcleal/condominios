import { UserService } from './../user.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { Component, OnInit } from '@angular/core';
import { Usuario } from '../model/usuario';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  state = '';
  error: any;

  constructor(private af: AngularFireAuth,
    private router: Router,
    private userService: UserService) { }

  ngOnInit() {
  }

  onSubmit(formData) {
    this.af.auth.createUserWithEmailAndPassword(formData.value.email, formData.value.password)
      .then((res) => {

        let usuario: Usuario = new Usuario();
        usuario.email = res.email;
        usuario.id = res.uid;
        usuario.idCondominio = '03FMss0QFzOcekQDZUsO';

        this.userService.criarUsuario(usuario).then((response) => {
          console.warn(response);
          this.userService.usuarioLogado = usuario;
          this.router.navigate(['auth']);
        });

      }).catch(err => {
        console.error(err);
        this.error = err;
      });
  }

}
