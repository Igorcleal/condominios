import { AngularFirestore } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';
import { Profissao } from '../model/profissao';
import { UserService } from '../user.service';

@Injectable()
export class ProfissoesService {

  constructor(private db: AngularFirestore,
    private userService: UserService) { }

  insertProfissao(profissao: Profissao) {
    return this.db.collection('condominios').doc(this.userService.getIdCondominioUsuarioLogado())
      .collection('profissoes').add(profissao.getData());
  }

  updateProfissao(profissao: Profissao) {
    console.log(this.userService.getIdCondominioUsuarioLogado());
    return this.db.collection('condominios')
      .doc(this.userService.getIdCondominioUsuarioLogado())
      .collection('profissoes')
      .doc(profissao.id)
      .update(profissao.getData());
  }

  getAll() {
    return this.db.collection('condominios').doc('03FMss0QFzOcekQDZUsO')
      .collection('profissoes', ref => {
        return ref.orderBy('dataCriacao');
      }).snapshotChanges().map(actions => {
        return actions.map(a => {
          console.log(a);
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
  }

}
