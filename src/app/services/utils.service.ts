import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class UtilsService {

  eventLoading = new EventEmitter<any>();

  constructor() { }

  showLoading() {
    this.eventLoading.emit(true);
  }

  hideLoading(){
    this.eventLoading.emit(false);
  }

}
