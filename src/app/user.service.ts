import { AngularFirestore } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';
import { Usuario } from './model/usuario';

@Injectable()
export class UserService {

  private isUserLoggedIn;
  private userName;
  usuarioLogado;

  constructor(private af: AngularFirestore) {
    this.isUserLoggedIn = false;
  }

  setUserLoggedIn() {
    this.isUserLoggedIn = true;
  }

  getUserLoggedIn() {
    return this.isUserLoggedIn;
  }

  getUsuarioLogado(): Usuario {
    return this.usuarioLogado;
  }

  getIdCondominioUsuarioLogado() {
    if (!this.usuarioLogado) {
      return '03FMss0QFzOcekQDZUsO';
    }
    return this.usuarioLogado.idCondominio;
  }

  recuperarUsuario(id) {
    this.af.collection('usuarios').doc(id).valueChanges().subscribe((user) => {
      this.usuarioLogado = user;
    })

  }

  criarUsuario(usuario: Usuario) {

    let promises = []

    promises.push(this.af.collection('condominios').doc(usuario.idCondominio)
      .collection('usuarios').doc(usuario.id)
      .set(usuario.getData()));

    promises.push(this.af.collection('usuarios').doc(usuario.id)
      .set(usuario.getData()));

    return Promise.all(promises)
  }

}
